FROM docker:20.10.8

RUN apk update && apk add --no-cache \
  git \
  openssl \
  openssl-dev \
  ca-certificates \
  bash \
  curl \
  make \
  gcc \
  g++ \
  python3 \
  python3-dev \
  py3-pip \
  linux-headers \
  paxctl \
  libgcc \
  libstdc++ \
  gnupg \
  nodejs-current \
  npm \
  krb5-libs \
  && update-ca-certificates \
  rust \
  docker-cli \
  libffi-dev \
  libc-dev \
  docker-compose

RUN apk update && apk add bash libc6-compat alpine-sdk
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools

# Add the patch fix
COPY ./stack-fix.c /lib/

RUN node -v && npm -v

#pnpm
ENV PNPM_HOME="/root/.local/share/pnpm/pnpm"
RUN mkdir -p ${PNPM_HOME}
ENV PATH="$PNPM_HOME:$PATH"
RUN curl -fsSL "https://github.com/pnpm/pnpm/releases/latest/download/pnpm-linuxstatic-x64" -o /bin/pnpm; chmod +x /bin/pnpm;
RUN pnpm -v

# Prepare the libraries packages
RUN set -ex \
    && apk add --no-cache  --virtual .build-deps build-base \
    && gcc  -shared -fPIC /lib/stack-fix.c -o /lib/stack-fix.so \
    && apk del .build-deps

# export the environment variable of LD_PRELOAD
ENV LD_PRELOAD /lib/stack-fix.so

RUN pnpm config set unsafe-perm true
