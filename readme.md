# @hosttoday/ht-docker-dbase
easy CI for docker images

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@hosttoday/ht-docker-dbase)
* [gitlab.com (source)](https://gitlab.com/hosttoday/ht-docker-dbase)
* [github.com (source mirror)](https://github.com/hosttoday/ht-docker-dbase)
* [docs (typedoc)](https://hosttoday.gitlab.io/ht-docker-dbase/)

## Status for master
[![build status](https://gitlab.com/hosttoday/ht-docker-dbase/badges/master/build.svg)](https://gitlab.com/hosttoday/ht-docker-dbase/commits/master)
[![coverage report](https://gitlab.com/hosttoday/ht-docker-dbase/badges/master/coverage.svg)](https://gitlab.com/hosttoday/ht-docker-dbase/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/@hosttoday/ht-docker-dbase.svg)](https://www.npmjs.com/package/@hosttoday/ht-docker-dbase)
[![Known Vulnerabilities](https://snyk.io/test/npm/@hosttoday/ht-docker-dbase/badge.svg)](https://snyk.io/test/npm/@hosttoday/ht-docker-dbase)
[![TypeScript](https://img.shields.io/badge/TypeScript->=%203.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-prettier-ff69b4.svg)](https://prettier.io/)

## Usage

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy.html)

[![repo-footer](https://hosttoday.gitlab.io/assets/repo-footer.svg)](https://maintainedby.lossless.com)
